import { themes as prismThemes } from 'prism-react-renderer';
import type { Config } from '@docusaurus/types';
import type * as Preset from '@docusaurus/preset-classic';

const config: Config = {
	title: '九头虫数据采集插件',
	tagline: '支持单页面采集、多页面采集、接口采集、自动化操作、导出数据（Excel、JSON、CSV）',
	favicon: 'img/logo.png',

	// Set the production url of your site here
	url: 'https://jtc.lhyhq.cn',
	// Set the /<baseUrl>/ pathname under which your site is served
	// For GitHub pages deployment, it is often '/<projectName>/'
	baseUrl: '/',

	// GitHub pages deployment config.
	// If you aren't using GitHub pages, you don't need these.
	organizationName: 'facebook', // Usually your GitHub org/user name.
	projectName: 'docusaurus', // Usually your repo name.

	onBrokenLinks: 'throw',
	onBrokenMarkdownLinks: 'warn',

	// Even if you don't use internationalization, you can use this field to set
	// useful metadata like html lang. For example, if your site is Chinese, you
	// may want to replace "en" with "zh-Hans".
	i18n: {
		defaultLocale: 'zh-Hans',
		locales: ['zh-Hans'],
	},

	presets: [
		[
			'classic',
			{
				docs: {
					sidebarPath: './sidebars.ts',
					// Please change this to your repo.
					// Remove this to remove the "edit this page" links.
					editUrl: 'https://gitee.com/whnba/jtc.addons.doc/tree/master/',
				},
				blog: false,
				// blog: {
				// 	showReadingTime: true,
				// 	// Please change this to your repo.
				// 	// Remove this to remove the "edit this page" links.
				// 	editUrl: "https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/",

				// },
				theme: {
					customCss: './src/css/custom.css',
				},
			} satisfies Preset.Options,
		],
	],
	scripts: [
		{
			src: 'https://umami.lhyhq.cn/umami.js',
			async: true,
			defer: true,
			'data-website-id': '0d48226b-161f-4745-97ee-54d941489dca',
		},
	],

	themeConfig: {
		// Replace with your project's social card
		image: 'img/docusaurus-social-card.jpg',
		colorMode: {
			disableSwitch: true,
		},

		navbar: {
			title: '九头虫数据采集插件',
			logo: {
				alt: '九头虫数据采集插件',
				src: 'img/logo.png',
			},
			items: [
				{
					type: 'docSidebar',
					sidebarId: 'tutorialSidebar',
					position: 'left',
					label: '操作文档',
				},
				{ to: '/author', label: '联系作者', position: 'left' },
				// {
				// 	type: "docsVersionDropdown",
				// },
			],
		},
		footer: {
			style: 'dark',

			copyright: `Copyright © ${new Date().getFullYear()} 九头虫浏览器数据采集插件`,
		},
		prism: {
			theme: prismThemes.github,
			darkTheme: prismThemes.dracula,
		},
	} satisfies Preset.ThemeConfig,
};

export default config;
