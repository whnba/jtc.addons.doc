---
sidebar_position: 3
---

# CSS选择器

* [CSS选择器参考手册](https://www.w3school.com.cn/cssref/css_selectors.asp)
* [CSS选择器类型（视频教程）](https://www.bilibili.com/video/BV1QW411C74G?spm_id_from=333.337.search-card.all.click&vd_source=aeafe42b530d49261580eadfbc0761d5)
* [F12打开浏览器开发者工具（视频教程）](https://www.bilibili.com/video/BV1hY4y1q73g?spm_id_from=333.337.search-card.all.click&vd_source=aeafe42b530d49261580eadfbc0761d5)
* [通过开发者工具查看元素信息（视频教程）](https://www.bilibili.com/video/BV19S4y1T7JM?spm_id_from=333.337.search-card.all.click&vd_source=aeafe42b530d49261580eadfbc0761d5)