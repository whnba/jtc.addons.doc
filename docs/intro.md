---
sidebar_position: 1
---

# 插件简介 

九头虫数据采集插件是一款功能全面的网页数据采集工具，旨在为用户提供便捷的数据抓取解决方案。无论是初学者还是专业人士，都可以通过这款插件高效地从各种网页中采集所需信息

## 视频教程

[https://www.bilibili.com/video/BV1Ty421q7Fb/?t=10&spm_id_from=333.1350.jump_directly](https://www.bilibili.com/video/BV1Ty421q7Fb/?t=10&spm_id_from=333.1350.jump_directly)

<iframe width="640" height="480" src="//player.bilibili.com/player.html?aid=1551541971&bvid=BV1Ty421q7Fb&cid=1464016567&p=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"> </iframe>

## 下载地址

* 🦊 [火狐浏览器扩展](https://addons.mozilla.org/zh-CN/firefox/addon/jtccj/)
* 🌐 [Edge浏览器扩展](https://microsoftedge.microsoft.com/addons/detail/%E4%B9%9D%E5%A4%B4%E8%99%AB%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86%EF%BC%88%E6%94%AF%E6%8C%81%E5%A4%9A%E7%BA%A7%E9%A1%B5%E9%9D%A2%E9%87%87%E9%9B%86%E5%8D%95%E9%A1%B5%E9%9D%A2%E9%87%87%E9%9B%86%E6%95%B0%E6%8D%AE/pbojbhbhgenkoebpdcpojfnohjdjckjb?hl=zh-CN)
* 🔍 [谷歌浏览器扩展](https://chromewebstore.google.com/detail/%E4%B9%9D%E5%A4%B4%E8%99%AB%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86%EF%BC%88%E6%94%AF%E6%8C%81%E5%A4%9A%E7%BA%A7%E9%A1%B5%E9%9D%A2%E9%87%87%E9%9B%86%E5%8D%95%E9%A1%B5%E9%9D%A2%E9%87%87%E9%9B%86%E6%95%B0%E6%8D%AE%E6%8E%A5%E5%8F%A3/dopdcakmheonlfplanpbeodjicmpalkm)
* 📦 [安装包下载(密码：x7em)](https://pan.baidu.com/s/1gTkq4ff132wh3K1LojGmHA)

## 核心功能
* 📑 **多页面采集**：用户可以设置多个页面的采集任务，插件会自动遍历并抓取数据。
* 📄 **单页面采集**：针对单一网页，快速提取文本、图片等信息。
* 🔗 **接口采集**：支持通过API接口获取数据，适用于动态网页或需要登录的网站。
* 🤖 **自动化操作**：插件能够模拟用户操作，如自动翻页、滚动、点击等，以实现更复杂的数据采集任务。
* ⏰ **计划任务**：允许用户设置定时采集任务，自动化地在预定时间执行数据抓取。
* 💾 **数据导出**：支持将采集到的数据导出为Excel、JSON、CSV等多种格式，方便用户进一步处理和分析。

## 使用场景
九头虫数据采集插件适用于多种数据采集需求，包括但不限于：

* 📊 市场研究
* 🏆 竞争分析
* 🎓 学术研究
* 📰 内容聚合
* 💰 产品比价

