import { useEffect } from "react";

export default function () {
	useEffect(() => {
		let timer = setTimeout(() => {
			window.location.href = "/";
		}, 1000 * 3);
		return () => clearTimeout(timer);
	}, []);

	return (
		<div style={{ margin: "50px auto" }}>
			<h3 style={{ textAlign: "center" }}>
				{3} 秒后奖跳转到帮助文档页面，立即跳转<a href="/">点击此处</a>
			</h3>
		</div>
	);
}
