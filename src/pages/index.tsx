import clsx from "clsx";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import Layout from "@theme/Layout";
import HomepageFeatures from "@site/src/components/HomepageFeatures";
import Heading from "@theme/Heading";
import styles from "./index.module.css";

function HomepageHeader() {
	const { siteConfig } = useDocusaurusContext();
	return (
		<header className={clsx("hero ", styles.heroBanner)}>
			<div style={{ margin: "0 auto" }}>
				<Heading as="h1" className={styles.title}>
					{siteConfig.title}
				</Heading>
				<p className={styles.subtitle}>{siteConfig.tagline}</p>
				<p style={{ color: "red" }}>注意：请根据您的浏览器下载对应的程序</p>
				<div className={styles.xiazaibtn}>
					<a className={styles.xiazaibtn_item} href="https://addons.mozilla.org/zh-CN/firefox/addon/jtccj/" target="_blank">
						<img src={require("@site/static/img/ff.png").default} title="火狐浏览器" />
						<h3>火狐浏览器</h3>
						<p>前往浏览器扩展中心下载插件</p>
					</a>
					<a
						className={styles.xiazaibtn_item}
						target="_blank"
						href="https://microsoftedge.microsoft.com/addons/detail/%E5%85%A8%E8%87%AA%E5%8A%A8%E7%BD%91%E9%A1%B5%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86%E6%8F%92%E4%BB%B6%E4%B9%9D%E5%A4%B4%E8%99%AB%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86/pbojbhbhgenkoebpdcpojfnohjdjckjb?hl=zh-CN"
					>
						<img src={require("@site/static/img/edge.png").default} title="Edge浏览器" />
						<h3>Edge浏览器</h3>
						<p>前往浏览器扩展中心下载插件</p>
					</a>
					<a
						className={styles.xiazaibtn_item}
						target="_blank"
						href="https://chrome.google.com/webstore/detail/%E4%B9%9D%E5%A4%B4%E8%99%AB%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86%EF%BC%88%E6%94%AF%E6%8C%81%E5%A4%9A%E7%BA%A7%E9%A1%B5%E9%9D%A2%E9%87%87%E9%9B%86%E5%8D%95%E9%A1%B5%E9%9D%A2%E9%87%87%E9%9B%86%E8%87%AA%E5%AE%9A%E4%B9%89%E8%84%9A/dopdcakmheonlfplanpbeodjicmpalkm"
					>
						<img src={require("@site/static/img/chrome.png").default} title="谷歌浏览器" />
						<h3>谷歌浏览器</h3>
						<p>前往浏览器扩展中心下载插件</p>
					</a>
					<a className={styles.xiazaibtn_item} href="https://pan.baidu.com/s/1gTkq4ff132wh3K1LojGmHA" target="_blank">
						<img src={require("@site/static/img/zip.png").default} title="安装包（密码：x7em）" />
						<h3>安装包（密码：x7em）</h3>
						<p>无法访问chrome应用商店的,点击此处下载自行安装</p>
					</a>
				</div>
			</div>
		</header>
	);
}

export default function () {
	const { siteConfig } = useDocusaurusContext();
	return (
		<Layout
			title={siteConfig.title}
			description="九头虫数据采集插件提供全面的数据采集解决方案，包括单页面采集、多页面采集和接口采集。此外，我们还支持自动化操作，确保数据采集过程高效且准确。最终，用户可以便捷地将数据导出为Excel、JSON或CSV格式，满足不同的数据应用需求"
		>
			<HomepageHeader />
			<main>
				<HomepageFeatures />
			</main>
		</Layout>
	);
}
