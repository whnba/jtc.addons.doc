import clsx from "clsx";
import Heading from "@theme/Heading";
import styles from "./styles.module.css";

type FeatureItem = {
	title: string;
	Svg: React.ComponentType<React.ComponentProps<"svg">>;
	description: JSX.Element;
};

const FeatureList: FeatureItem[] = [
	{
		title: "永久免费",
		Svg: require("@site/static/img/yjmf.svg").default,
		description: <>无需注册，即刻享用所有功能。完全免费，无隐藏费用。</>,
	},
	{
		title: "操作简单",
		Svg: require("@site/static/img/czjd.svg").default,
		description: <>基于CSS选择器，无需编写任何代码，即可精准捕获所需数据，让数据采集变得既高效又便捷。</>,
	},
	{
		title: "自动化采集",
		Svg: require("@site/static/img/zdh.svg").default,
		description: <>减少重复性工作，让用户能够更专注于创造性任务。自动化操作不仅提高了效率，还为用户提供了更加轻松的使用体验。</>,
	},
	{
		title: "数据导出",
		Svg: require("@site/static/img/sjdc.svg").default,
		description: <>采集到的数据可导出为：Excel、JSON、CSV等数据格式，你可根据需求来下载不同格式数据。</>,
	},
	{
		title: "计划任务",
		Svg: require("@site/static/img/jhrw.svg").default,
		description: <>用户创建和管理定时执行的任务。这些任务可以是单次的或者重复的，用户可以设置具体的执行时间。</>,
	},
];

function Feature({ title, Svg, description }: FeatureItem) {
	return (
		<div className={clsx("col col--4")}>
			<div className='text--center'>
				<Svg className={styles.featureSvg} role='img' />
			</div>
			<div className='text--center padding-horiz--md'>
				<Heading as='h3'>{title}</Heading>
				<p>{description}</p>
			</div>
		</div>
	);
}

export default function HomepageFeatures(): JSX.Element {
	return (
		<section className={styles.features}>
			<div className='container'>
				<div className='row'>
					{FeatureList.map((props, idx) => (
						<Feature key={idx} {...props} />
					))}
				</div>
			</div>
		</section>
	);
}
